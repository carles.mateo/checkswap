#!/usr/bin/env python3
import sys

from lib.datetimeutils import DateTimeUtils
from lib.fileutils import FileUtils
from lib.pythonutils import PythonUtils
from lib.systemutils import SystemUtils


class Checkswap:

    s_version = "0.2"
    s_file_swap_info = "/proc/vmstat"
    i_page_size_kb = 4

    def __init__(self, o_pythonutils, o_fileutils, o_datetimeutils, o_systemutils):
        self.o_pythonutils = o_pythonutils
        self.o_fileutils = o_fileutils
        self.o_datetimeutils = o_datetimeutils
        self.o_systemutils = o_systemutils

        self.a_s_vmstat_lines = []

        # Init array to 300 positions, 5 minutes
        self.a_i_vmstat_pswpin_reads = []
        self.a_i_vmstat_pswpout_reads = []
        for i_number in range(0, 300):
            self.a_i_vmstat_pswpin_reads.append(0)
            self.a_i_vmstat_pswpout_reads.append(0)
        self.i_pointer_to_vmstat_reads = 0
        self.b_array_full_read = False

        # Just init the variables
        self.i_vmstat_pswpin_first_read = 0
        self.i_vmstat_pswpout_first_read = 0
        self.i_vmstat_pswpin_last_read = 0
        self.i_vmstat_pswpout_last_read = 0

        self.i_vmstat_pswpin_max_read = 0
        self.i_vmstat_pswpout_max_read = 0

    def print_help(self):
        print("checkswap.py by Carles Mateo")
        print("")
        print("Some info around the metrics from:")
        print("https://superuser.com/questions/785447/what-is-the-exact-difference-between-the-parameters-pgpgin-pswpin-and-pswpou")
        s_message = "Paging refers to writing portions, termed pages, of a process' memory to disk.\n"
        s_message += "Swapping, strictly speaking, refers to writing the entire process, not just part, to disk.\n"
        s_message += "In Linux, true swapping is exceedingly rare, but the terms paging and swapping often are used interchangeably.\n"
        print(s_message)
        print("")

        s_message = 'page-out: The system\'s free memory is less than a threshold "lotsfree" and unnused / least used pages are moved to the swap area.' + "\n"
        s_message += "page-in: One process which is running requested for a page that is not in the current memory (page-fault), it's pages are being brought back to memory.\n"
        s_message += "swap-out: System is thrashing and has deactivated a process and it's memory pages are moved into the swap area.\n"
        s_message += "swap-in: A deactivated process is back to work and it's pages are being brought into the memory."
        print(s_message)
        print("")

        print("Most common page size in x86_64 architecture has 4KB size.")
        print("")
        s_message = "Values from /proc/vmstat:\n"
        s_message += "* pgpgin, pgpgout - number of pages that are read from disk and written to memory, you usually don't need to care that much about these numbers\n"
        s_message += "* pswpin, pswpout - you may want to track these numbers per time (via some monitoring like prometheus), if there are spikes it means system is heavily swapping and you have a problem.\n"
        print(s_message)
        print("")

    def add_vmstat_read(self, i_pswpin, i_pswpout):
        """
        Adds to the buffer of the last 300 seconds readings
        :param i_pswpin:
        :param i_pswpout:
        :return: i_pswpin and i_pswpout normalized after disccounting the initial values
        """

        if self.b_array_full_read is False and self.i_pointer_to_vmstat_reads == 0:
            # That was the firs read
            self.i_vmstat_pswpin_first_read = i_pswpin
            self.i_vmstat_pswpout_first_read = i_pswpout
            # Those contains the full value read from the Kernel since System start
            self.i_vmstat_pswpin_last_read = i_pswpin
            self.i_vmstat_pswpout_last_read = i_pswpout

        i_pswpin_new = i_pswpin - self.i_vmstat_pswpin_last_read
        i_pswpout_new = i_pswpout - self.i_vmstat_pswpout_last_read

        if i_pswpin_new > self.i_vmstat_pswpin_max_read:
            self.i_vmstat_pswpin_max_read = i_pswpin_new
        if i_pswpout_new > self.i_vmstat_pswpout_max_read:
            self.i_vmstat_pswpout_max_read = i_pswpout_new

        self.a_i_vmstat_pswpin_reads[self.i_pointer_to_vmstat_reads] = i_pswpin_new
        self.a_i_vmstat_pswpout_reads[self.i_pointer_to_vmstat_reads] = i_pswpout_new
        self.i_pointer_to_vmstat_reads = self.i_pointer_to_vmstat_reads + 1

        if self.i_pointer_to_vmstat_reads > 299:
            self.i_pointer_to_vmstat_reads = 0
            # We got 5 minutes reads already
            self.b_array_full_read = True

        # Those contains the full value read from the Kernel since System start
        self.i_vmstat_pswpin_last_read = i_pswpin
        self.i_vmstat_pswpout_last_read = i_pswpout

        # print("Debug: ", i_pswpin_new, i_pswpout_new, i_pswpin, i_pswpout, self.i_vmstat_pswpin_last_read, self.i_vmstat_pswpout_last_read)

        return i_pswpin_new, i_pswpout_new

    def get_avg_vmstat_pswpin_pswpout(self):
        i_total_pswpin = 0
        i_total_pswpout = 0

        if self.b_array_full_read is False:
            i_secs = self.i_pointer_to_vmstat_reads
        else:
            i_secs = 300

        for i_index in range(0, i_secs):
            i_total_pswpin = i_total_pswpin + self.a_i_vmstat_pswpin_reads[i_index]
            i_total_pswpout = i_total_pswpout + self.a_i_vmstat_pswpout_reads[i_index]
        i_avg_pswpin = int(i_total_pswpin / i_secs)
        i_avg_pswpout = int(i_total_pswpout / i_secs)

        return i_secs, i_avg_pswpin, i_avg_pswpout

    def get_key_value(self, s_key):
        """
        Beware of false positives, that's why I add " " additional space at the end
        :return: b_success, s_value
        """
        for s_line in self.a_s_vmstat_lines:
            if s_key + " " in s_line:
                a_key_value = s_line.split(" ")
                s_value = a_key_value[1]
                s_value = s_value.strip()

                return True, s_value

        # Not Found. That's an error condition
        return False, ""

    def get_pswpout(self):
        b_success, s_value = self.get_key_value("pswpout")

        return b_success, s_value

    def get_pswpin(self):
        """

        :return: b_success, s_value
        """
        b_success, s_value = self.get_key_value("pswpin")

        return b_success, s_value

    def read_vmstat_file(self):

        b_success, a_s_lines = o_fileutils.read_file_as_lines(self.s_file_swap_info)

        return b_success, a_s_lines

    def p(self, s_text):
        s_datetime = self.o_datetimeutils.get_datetime(b_milliseconds=False, b_remove_spaces_and_colons=False)
        print(s_datetime + " " + s_text)

    def main(self):
        while True:
            b_success, self.a_s_vmstat_lines = self.read_vmstat_file()
            if b_success is False:
                print("Error accessing " + self.s_file_swap_info)
                sys.exit(1)
            b_success, s_pswpin = self.get_pswpin()
            if b_success is False:
                print("Error reading pswpin value")
                sys.exit(1)
            b_success, s_pswpout = self.get_pswpout()
            if b_success is False:
                print("Error reading pswpout value")
                sys.exit(1)

            # Add Reads and update the counter
            i_pswpin, i_pswpout = self.add_vmstat_read(int(s_pswpin), int(s_pswpout))
            # Overwrite the old values with the new, relatives to last read value
            s_pswpin = str(i_pswpin)
            s_pswpout = str(i_pswpout)

            i_pswpin_kb = i_pswpin * self.i_page_size_kb
            i_pswpout_kb = i_pswpout * self.i_page_size_kb

            # First time info
            if self.b_array_full_read is False and self.i_pointer_to_vmstat_reads == 1:
                self.print_help()

                print("Initial values since System start:")
                i_length = len(str(self.i_vmstat_pswpout_first_read))
                if len(str(self.i_vmstat_pswpin_first_read)) > len(str(self.i_vmstat_pswpout_first_read)):
                    i_length = len(str(self.i_vmstat_pswpin_first_read))

                self.p("pswpin : " + str(self.i_vmstat_pswpin_first_read).rjust(i_length))
                self.p("pswpout: " + str(self.i_vmstat_pswpout_first_read).rjust(i_length))
                print("")
                print("-" * 50)
                print("")

            i_secs, i_avg_pswpin, i_avg_pswpout = self.get_avg_vmstat_pswpin_pswpout()

            i_length = len(s_pswpout)
            if len(s_pswpin) > len(s_pswpout):
                i_length = len(s_pswpin)
            i_length_kb = len(str(i_pswpout_kb))
            if len(str(i_pswpin_kb)) > len(str(i_pswpout_kb)):
                i_length_kb = len(str(i_pswpin_kb))

            s_message_pswpin = "pswpin : " + s_pswpin.rjust(i_length) + " pages" + " (" + str(i_pswpin_kb).rjust(i_length_kb) + " KB)."
            s_message_pswpout = "pswpout: " + s_pswpout.rjust(i_length) + " pages" + " (" + str(i_pswpout_kb).rjust(i_length_kb) + " KB)."

            s_message_pswpin_max = "Max: " + str(self.i_vmstat_pswpin_max_read) + " pages."
            s_message_pswpout_max = "Max: " + str(self.i_vmstat_pswpout_max_read) + " pages."

            self.p(s_message_pswpin + " Avg for last " + str(i_secs) + " s. : " + str(i_avg_pswpin) + " pages. " + s_message_pswpin_max)
            self.p(s_message_pswpout + " Avg for last " + str(i_secs) + " s. : " + str(i_avg_pswpout) + " pages. " + s_message_pswpout_max)
            print("")

            self.o_systemutils.sleep(1)


if __name__ == "__main__":

    try:

        o_pythonutils = PythonUtils()
        o_fileutils = FileUtils(o_pythonutils=o_pythonutils)
        o_datetimeutils = DateTimeUtils()
        o_systemutils = SystemUtils()

        o_checkswap = Checkswap(o_pythonutils=o_pythonutils, o_fileutils=o_fileutils, o_datetimeutils=o_datetimeutils, o_systemutils=o_systemutils)
        o_checkswap.main()

    except KeyboardInterrupt:
        print("CTRL + C received. Exiting checkswap.py...")
        sys.exit(0)
    except:
        print("Exception catched.")

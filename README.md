# checkswap

by Carles Mateo

Version 0.3

This Open Source program tracks the usage of the Swap Pages on Linux System.

The idea is to help to monitor how much the pages are being read/pushed to swap, and what's the IO generated to the drives.

#
# Class to work with Typical System Operations, like exiting the App with an error code
#
# Author: Carles Mateo
# Creation Date: 2014-01-20 17:02 Barcelona
#

import time
# from lib.screenutils import ScreenUtils
# from lib.datetimeutils import DateTimeUtils


class SystemUtils:

    def sleep(self, i_seconds=1):
        """
        Sleep the number of seconds indicated
        :param i_seconds:
        :return:
        """
        time.sleep(i_seconds)
